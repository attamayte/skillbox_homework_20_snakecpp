// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/InputComponent.h"

#include "SnakePlayerPawn.generated.h"

class UCameraComponent;
class ASnake;

UCLASS()
class SNAKECPP_API ASnakePlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASnakePlayerPawn();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* Camera;

	UPROPERTY(BlueprintReadWrite) // do we really need Write?
	ASnake* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnake> SnakeActorClass;

	void SpawnSnakeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UFUNCTION()
	void HandleInputVerticalAxis(float input);
	UFUNCTION()
	void HandleInputHorizontalAxis(float input);

};
