// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExampleActor.generated.h"


UENUM(BlueprintType)
enum class EReasonsToHateHungarianNotation : uint8
{
	RTHHN_COMPILERS_ALREADY_DO_TYPE_CHECKING		UMETA(DisplayName = "WHY THE HECK SHOULD YOU"),
	RTHHN_MODERN_IDE_SHOWS_TYPE_INFO				UMETA(DisplayName = "MAKE A NAME FRIENDLIER HERE"),
	RTHHN_HARDER_TO_CHANGE_TYPE_LATER				UMETA(DisplayName = "RATHER THAN COME UP WITH A"),
	RTHHN_MAKES_CODE_LESS_READABLE					UMETA(DisplayName = "GOOD ONE FROM THE BEGINNING?")
};

USTRUCT(BlueprintType)
struct FQuestInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Reward { 0 };		// gold pieces
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Name {TEXT("Quest name is undefined")};
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* Item { nullptr };
};

UCLASS()
class SNAKECPP_API AExampleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExampleActor();

	UPROPERTY(BlueprintReadOnly)
	EReasonsToHateHungarianNotation MyReason;
	UPROPERTY(BlueprintReadWrite)
	FQuestInfo CurrentQuest;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
