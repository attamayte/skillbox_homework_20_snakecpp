// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

class ASnakeElem;

namespace SnakeDirection
{
	const FVector UP		= { 1.F,	0.F,	0.F };
	const FVector DOWN		= { -1.F,	0.F,	0.F };
	const FVector RIGHT		= { 0.F,	1.F,	0.F };
	const FVector LEFT		= { 0.F,	-1.F,	0.F };
}

UCLASS()
class SNAKECPP_API ASnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElem> SnakeElemClass;

	UPROPERTY()
	TArray<ASnakeElem*> Body;

	UPROPERTY(EditDefaultsOnly)
	float ElemSize = 100.F;

	UPROPERTY(EditDefaultsOnly)
	float MoveInterval = 1.F;

	UPROPERTY(EditDefaultsOnly)
	UStaticMesh* HeadMesh = nullptr;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* HeadMaterial = nullptr;

	UPROPERTY()
	FVector Direction = SnakeDirection::DOWN;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Grow(const size_t size = 1U);
	void Move();
};