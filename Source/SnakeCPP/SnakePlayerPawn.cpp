// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePlayerPawn.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Snake.h"

// Sets default values
ASnakePlayerPawn::ASnakePlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
}

// Called when the game starts or when spawned
void ASnakePlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));

	SpawnSnakeActor();
}

void ASnakePlayerPawn::SpawnSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnake>(SnakeActorClass, FTransform());
}

// Called every frame
void ASnakePlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASnakePlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &ASnakePlayerPawn::HandleInputVerticalAxis);
	PlayerInputComponent->BindAxis("Horizontal", this, &ASnakePlayerPawn::HandleInputHorizontalAxis);
}

void ASnakePlayerPawn::HandleInputVerticalAxis(float input)
{
	if (IsValid(SnakeActor) && SnakeActor->Direction.X == 0.F)
	{
		if (input > 0.F) SnakeActor->Direction = SnakeDirection::UP;
		else if (input < 0.F) SnakeActor->Direction = SnakeDirection::DOWN;
	}
}

void ASnakePlayerPawn::HandleInputHorizontalAxis(float input)
{
	if (IsValid(SnakeActor) && SnakeActor->Direction.Y == 0.F)
	{
		if (input > 0.F) SnakeActor->Direction = SnakeDirection::RIGHT;
		else if (input < 0.F) SnakeActor->Direction = SnakeDirection::LEFT;
	}
}

