// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElem.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MoveInterval);
	Grow(4U);
	if (HeadMesh) Body[0]->Mesh->SetStaticMesh(HeadMesh);
	if (HeadMaterial) Body[0]->Mesh->SetMaterial(0, HeadMaterial);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnake::Grow(const size_t size)
{
	auto Pos = GetActorTransform();
	auto Root = GetWorld();
	for (auto i = 0; i < size; ++i)
	{
		Body.Add(Root->SpawnActor<ASnakeElem>(SnakeElemClass, Pos));
		Pos.AddToTranslation(FVector(ElemSize, 0, 0));
	}
}

void ASnake::Move()
{
	for (auto i = Body.Num() - 1; i; --i)
		Body[i]->SetActorLocation(Body[i - 1]->GetActorLocation());
	Body[0]->AddActorWorldOffset(Direction * ElemSize);
}