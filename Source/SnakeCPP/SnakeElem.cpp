// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElem.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeElem::ASnakeElem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
}

// Called when the game starts or when spawned
void ASnakeElem::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeElem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

